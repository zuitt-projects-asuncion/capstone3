import bcrypt from 'bcryptjs';
const data = {
  users: [
    {
      name: 'Kilgor',
      email: 'admin@example.com',
      password: bcrypt.hashSync('123456'),
      isAdmin: true,
    },
    {
      name: 'John',
      email: 'user@example.com',
      password: bcrypt.hashSync('123456'),
      isAdmin: false,
    },
  ],
  products: [
    {
      //_id: '1',
      name: 'Threezero One Punch Man Saitama Season 2 1/6 3Z0134t',
      slug: '3zero-Fig-ONEsixth',
      category: 'figure',
      image: '/images/p1.jpg', // 679px × 829px
      price: 120,
      countInStock: 10,
      brand: 'Threezero',
      rating: 4.5,
      numReviews: 10,
      description:
        'This 1/6 Articulated Figure: Saitama (SEASON 2)” is approximately 30cm tall fully articulated figure. Its hero suit costume and cape is made of fabric, and has weathering effects applied. Comes with a serious expression head and 2 kinds of interchangeable gloved hands (1 pair of opened hands and 1 pair of fists).ality shirt',
    },
    {
      //_id: '2',
      name: 'KILLERBODY TRANSFORMERS BUMBLEBEE: WEARABLE HELMET SPEAKER TOUCH CONTROL OPTIMUS PRIME KB200069-14',
      slug: 'KB-helmet-lifescale',
      category: 'wearable',
      image: '/images/p2.jpg',
      price: 250,
      countInStock: 0,
      brand: 'Adidas',
      rating: 4.0,
      numReviews: 10,
      description:
        'Helmet Materials: ABS, PC, PVC, metal & Electronic components with display stand and materials made of ABS Silica gel with a metal counterweight',
    },
    {
      //_id: '3',
      name: 'Iron Studios Avengers: Endgame I Am Iron Man BDS Art Scale 1/10 MARCAS21519-10',
      slug: 'IS-Stat-ONEtenth',
      category: 'statue',
      image: '/images/p3.jpg',
      price: 25,
      countInStock: 15,
      brand: 'Iron Studios',
      rating: 4.5,
      numReviews: 14,
      description:
        'Limited edition statue commemorates the heroic moment of Tony in Avengers Endgame, Based on original movie references, Hand painted, Made in polystone, Automotive paint',
    },
    {
      //_id: '4',
      name: 'Hot Toys Avengers: Endgame Rocket Collectible Figure 1/6 MMS548',
      slug: 'HT-Fig-ONEsixth',
      category: 'figure',
      image: '/images/p4.jpg',
      price: 65,
      countInStock: 5,
      brand: 'Hot Toys',
      rating: 4.5,
      numReviews: 10,
      description:
        'Approximately 16cm tall, Sculpted by So-Young, 1/6th and articulated',
    },
  ],
};
export default data;
